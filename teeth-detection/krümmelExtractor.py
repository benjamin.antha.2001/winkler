import cv2
import os

def save_frame(frame, folder, frame_count, video):
    filename = f"frame_{video}_{frame_count}.png"
    filepath = os.path.join(folder, filename)
    cv2.imwrite(filepath, frame)
    print(f"Frame {frame_count} saved as {filename}")

def main(video_path, output_folder, video):
    os.makedirs(output_folder, exist_ok=True)

    cap = cv2.VideoCapture(video_path)

    if not cap.isOpened():
        print("Error: Couldn't open the video file")
        return

    frame_count = 0

    while cap.isOpened():
        ret, frame = cap.read()

        if not ret:
            break

        # Show the frame
        cv2.imshow('Video', frame)

        # Check for space key press
        key = cv2.waitKey(1)
        if key == ord(' '):
            save_frame(frame, output_folder, frame_count, video)
            frame_count += 1
        elif key == 27:  # Esc key to exit
            break

    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    video = "09"
    video_path = "krümmel/"+video+".mov"  
    output_folder = "output_krümmel" 
    main(video_path, output_folder, video)
