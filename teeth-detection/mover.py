import os
import shutil

krummel_folder = "output_krümmel"
frames_folder = "output_frames"
text_folder = "annotations_v1"
output_folder = "output_combined"

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

text_files = os.listdir(text_folder)
text_file_names = [os.path.splitext(filename)[0] for filename in text_files]

def move_image_files(source_folder, destination_folder):
    for filename in os.listdir(source_folder):
        if filename.endswith((".jpg", ".png")):
            file_name_without_extension = os.path.splitext(filename)[0]
            if file_name_without_extension in text_file_names:
                shutil.move(os.path.join(source_folder, filename), os.path.join(destination_folder, filename))

move_image_files(krummel_folder, output_folder)

move_image_files(frames_folder, output_folder)
