import torch
import cv2
import time
import numpy as np
import math

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
path = '../models/run2_objectdetection3labels/weights/best.pt'
model = torch.hub.load("../yolov7","custom",f"{path}",force_reload=True, source='local',trust_repo=True)
model.conf = 0.7
model.to(device)
cap = cv2.VideoCapture('../videos/video_05.mp4')  # Or use '0' for webcam

prev_frame_time = 0
new_frame_time = 0
fps = 0
frame_count = 0
threshold_min = 0
threshold_max = 24
threshold_hough = 50
nr_of_skipping_frames = 30
cv2.namedWindow('Frame')

def update_threshold_min(val):
    global threshold_min
    threshold_min = val

def update_threshold_max(val):
    global threshold_max
    threshold_max = val

def update_threshold_hough(val):
    global threshold_hough
    threshold_hough = val

def calculate_angle_to_xaxis(pt1, pt2):
    m = (pt2[1] - pt1[1]) / (pt2[0] - pt1[0])
    angle_in_rad = np.arctan(m)
    return abs(math.degrees(angle_in_rad))

def calculate_lines(tooth_frame):
    tooth_frame = cv2.cvtColor(tooth_frame, cv2.COLOR_BGR2GRAY)
    tooth_frame = cv2.GaussianBlur(tooth_frame, (9,9), 0)
    edge_frame = cv2.Canny(tooth_frame, threshold_min, threshold_max, None, 3)
    return cv2.HoughLines(edge_frame, 1, 0.4 * np.pi / 180, threshold_hough,min_theta = 2.5, max_theta=3.2), edge_frame

def get_leftmost_line(lines):
    leftmost_line = None
    leftmost_rho = float('inf')

    if lines is not None:
        for i in range(len(lines)):
            rho = lines[i][0][0]

            if rho < leftmost_rho:
                leftmost_rho = rho
                leftmost_line = lines[i]

    if leftmost_line is not None:
        rho = leftmost_line[0][0]
        theta = leftmost_line[0][1]
        a = math.cos(theta)
        b = math.sin(theta)
        x0 = a * rho
        y0 = b * rho
        pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
        pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
        return (pt1, pt2)
    
    return (None, None)
    
def draw_line(pt1, pt2, frame):
    cv2.line(frame, pt1, pt2, (255,255,0), 1, cv2.LINE_AA)

def draw_prediction(frame, row):
    class_id = int(row['class'])
    confidence = float(row['confidence'])
    class_name = model.names[class_id]
    if class_id == 0:
        color = (255, 255, 0)
    elif class_id == 1:
        color = (255, 0, 0)
    else:
        color = (255, 0, 255)
    cv2.rectangle(frame, (int(row['xmin']), int(row['ymin'])),(int(row['xmax']), int(row['ymax'])), color, 2)
    cv2.putText(frame, f"{class_name} {confidence:.2f}", (int(row['xmin']), int(row['ymin']) - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

def get_roi(row):
    roi = frame[int(row['ymin']):int(row['ymax']), int(row['xmin']): int(row['xmax'])]
    roi_height = int(row['ymax']) - int(row['ymin'])
    return roi[:roi_height // 2, :]

def calculate_fps():
    global new_frame_time
    global prev_frame_time
    
    new_frame_time = time.time()

    if (new_frame_time-prev_frame_time) != 0:
        fps = 1 / (new_frame_time-prev_frame_time)
        prev_frame_time = new_frame_time
        return int(fps)

cv2.createTrackbar('Threshold Min', 'Frame', threshold_min, 255, update_threshold_min)
cv2.createTrackbar('Threshold Max', 'Frame', threshold_max, 255, update_threshold_max)
cv2.createTrackbar('Threshold h', 'Frame', threshold_hough, 255, update_threshold_hough)

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break

    frame_count += 1
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    if frame_count % nr_of_skipping_frames == 0:
        results = model(frame)
        df = results.pandas().xyxy[0]

        if not df.empty:
            min_xmin_index = df['xmin'].idxmin()
            row_with_least_xmin = df.loc[min_xmin_index]
            class_id_most_left = int(row_with_least_xmin['class'])
        else:
            continue

        if class_id_most_left == 0 and not df.empty:
            roi = get_roi(row_with_least_xmin)
            lines, edge_frame = calculate_lines(roi)
            pt1, pt2 = get_leftmost_line(lines)
            if pt1 and pt2 is not None:
                angle = calculate_angle_to_xaxis(pt1, pt2)
                print(angle)

            draw_line(pt1, pt2, edge_frame)
            cv2.imshow('Tooth', edge_frame)

        for _, row in df.iterrows():
            draw_prediction(frame, row)

    cv2.imshow('Frame', frame)  

    # print(calculate_fps())
                
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    
cap.release()
cv2.destroyAllWindows()
