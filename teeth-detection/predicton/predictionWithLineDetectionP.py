import torch
import cv2
import time
import cython
import numpy as np
import math

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
path = '../models/run2_objectdetection3labels/weights/best.pt'
model = torch.hub.load("../yolov7","custom",f"{path}",force_reload=True, source='local',trust_repo=True)
model.conf = 0.7
model.to(device)
cap = cv2.VideoCapture('../videos/video_05.mp4')  # Or use '0' for webcam
# cap.set(cv2.CAP_PROP_FPS, 36)

prev_frame_time: cython.int = 0
new_frame_time: cython.int = 0
fps: cython.int = 0
frame_count: cython.int = 0
threshold_min = 30
threshold_max = 80
threshold_hough = 30
cv2.namedWindow('Frame')

def update_threshold_min(val):
    global threshold_min
    threshold_min = val

def update_threshold_max(val):
    global threshold_max
    threshold_max = val

def update_threshold_hough(val):
    global threshold_hough
    threshold_hough = val

# Create trackbars for threshold_min and threshold_max
cv2.createTrackbar('Threshold Min', 'Frame', threshold_min, 255, update_threshold_min)
cv2.createTrackbar('Threshold Max', 'Frame', threshold_max, 255, update_threshold_max)
cv2.createTrackbar('Threshold h', 'Frame', threshold_hough, 255, update_threshold_hough)

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break

    frame_count += 1
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    if frame_count % 35 == 0:
        results = model(frame)
        # get coordinates for drawing the bounding boxes
        df: cython.array = results.pandas().xyxy[0]
        if not df.empty:
            min_xmin_index = df['xmin'].idxmin()
            row_with_least_xmin = df.loc[min_xmin_index]
            class_id = int(row_with_least_xmin['class'])
        else:
            continue
        # draw the bounding boxes
        if class_id == 0 and not df.empty:
            roi_image = frame[int(row_with_least_xmin['ymin']):int(row_with_least_xmin['ymax']), int(row_with_least_xmin['xmin']): int(row_with_least_xmin['xmax'])]
            roi_image = cv2.cvtColor(roi_image, cv2.COLOR_RGB2GRAY)
            roi_image = cv2.GaussianBlur(roi_image, (5,5), 0) 
            #canny
            dst = cv2.Canny(roi_image, threshold_min, threshold_max, None, 3)
            cdst = cv2.cvtColor(dst, cv2.COLOR_GRAY2BGR)
            cdstP = np.copy(cdst)
            linesP = cv2.HoughLinesP(dst, 1, np.pi / 180, threshold_hough, None, 60, 10)
            if linesP is not None:
                    for i in range(0, len(linesP)):
                        l = linesP[i][0]
                        p1 = [l[0], l[1]]
                        p2 = [l[2], l[3]]
                        m = (p2[1] - p1[1]) / (p2[0] - p1[0])
                        angle_in_rad = np.arctan(m)
                        angle_in_deg = math.degrees(angle_in_rad)
                        angle_to_vertical_line = 90 - angle_in_deg
                        print("p1", p1)
                        print("p2", p2)
                        print("m", m)
                        print("angle",abs(angle_to_vertical_line))
                        cv2.line(cdstP, (l[0], l[1]), (l[2], l[3]), (255,0,0), 1, cv2.LINE_AA) 
            cv2.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)

        for _, row in df.iterrows():
                class_id = int(row['class'])
                confidence = float(row['confidence'])
                class_name = model.names[class_id]
                if class_id == 0:
                    color = (255, 255, 0)
                elif class_id == 1:
                    color = (255, 0, 0)
                else:
                    color = (255, 0, 255)
                cv2.rectangle(frame, (int(row['xmin']), int(row['ymin'])),(int(row['xmax']), int(row['ymax'])), color, 2)
                cv2.putText(frame, f"{class_name} {confidence:.2f}", (int(row['xmin']), int(row['ymin']) - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    cv2.imshow('Frame', frame)  

    # calculate Frames
    new_frame_time = time.time()
    
    if (new_frame_time-prev_frame_time) != 0:
        fps = 1 / (new_frame_time-prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)

    print(fps)
                
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    

cap.release()
cv2.destroyAllWindows()
