import torch
import cv2
import time

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
path = '../models/run2_objectdetection3labels/weights/best.pt'
model = torch.hub.load("../yolov7","custom",f"{path}",force_reload=True, source='local',trust_repo=True)
model.conf = 0.7
model.to(device)
cap = cv2.VideoCapture('../videos/video_05.mp4')  # Or use '0' for webcam
# cap.set(cv2.CAP_PROP_FPS, 36)

prev_frame_time = 0
new_frame_time = 0
fps = 0
frame_count = 0

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break

    frame_count += 1
    
    if frame_count % 1 == 0:
        results = model(frame)
        # get coordinates for drawing the bounding boxes
        df = results.pandas().xyxy[0]
        # draw the bounding boxes
        for _, row in df.iterrows():
                class_id = int(row['class'])
                confidence = float(row['confidence'])
                class_name = model.names[class_id]
                if class_id == 0:
                    color = (255, 255, 0)
                elif class_id == 1:
                    color = (255, 0, 0)
                else:
                    color = (255, 0, 255)
                roi_image = frame[int(row['ymin']):int(row['ymax']), int(row['xmin']): int(row['xmax'])]
                cv2.imwrite("output.png",roi_image)
                cv2.rectangle(frame, (int(row['xmin']), int(row['ymin'])),(int(row['xmax']), int(row['ymax'])), color, 2)
                cv2.putText(frame, f"{class_name} {confidence:.2f}", (int(row['xmin']), int(row['ymin']) - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

    cv2.imshow('Frame', frame)  

    # calculate Frames
    new_frame_time = time.time()
    
    if (new_frame_time-prev_frame_time) != 0:
        fps = 1 / (new_frame_time-prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)

    print(fps)
                
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    

cap.release()
cv2.destroyAllWindows()
