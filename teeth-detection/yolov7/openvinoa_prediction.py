import numpy as np
import torch
from PIL import Image
from utils.datasets import letterbox
from utils.plots import plot_one_box
from typing import List, Tuple, Dict
from utils.general import scale_coords, non_max_suppression
from openvino.runtime import Model
from pathlib import Path
import cv2
import time
from openvino.runtime import serialize
from openvino.runtime import Core
import math
import pandas as pd

cap = cv2.VideoCapture('../videos/video_05.mp4')  # Or use '0' for webcam
cap.set(cv2.CAP_PROP_FPS, 24)
prev_frame_time = 0
new_frame_time = 0
fps = 0
frame_count = 0
threshold_min = 0
threshold_max = 24
threshold_hough = 50
nr_of_skipping_frames = 30

cv2.namedWindow('Frame')

def update_threshold_min(val):
    global threshold_min
    threshold_min = val

def update_threshold_max(val):
    global threshold_max
    threshold_max = val

def update_threshold_hough(val):
    global threshold_hough
    threshold_hough = val

def calculate_angle_to_xaxis(pt1, pt2):
    m = (pt2[1] - pt1[1]) / (pt2[0] - pt1[0])
    angle_in_rad = np.arctan(m)
    return abs(math.degrees(angle_in_rad))

def calculate_lines(tooth_frame):
    tooth_frame = cv2.cvtColor(tooth_frame, cv2.COLOR_BGR2GRAY)
    tooth_frame = cv2.GaussianBlur(tooth_frame, (9,9), 0)
    edge_frame = cv2.Canny(tooth_frame, threshold_min, threshold_max, None, 3)
    return cv2.HoughLines(edge_frame, 1, 0.4 * np.pi / 180, threshold_hough,min_theta = 2.5, max_theta=3.2), edge_frame

def get_leftmost_line(lines):
    leftmost_line = None
    leftmost_rho = float('inf')

    if lines is not None:
        for i in range(len(lines)):
            rho = lines[i][0][0]

            if rho < leftmost_rho:
                leftmost_rho = rho
                leftmost_line = lines[i]

    if leftmost_line is not None:
        rho = leftmost_line[0][0]
        theta = leftmost_line[0][1]
        a = math.cos(theta)
        b = math.sin(theta)
        x0 = a * rho
        y0 = b * rho
        pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
        pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
        return (pt1, pt2)
    
    return (None, None)
    
def draw_line(pt1, pt2, frame):
    cv2.line(frame, pt1, pt2, (0,0,255), 1, cv2.LINE_AA)

def get_roi(row):
    roi = frame[int(row[1]):int(row[3]), int(row[0]): int(row[2])]
    roi_height = int(row[3]) - int(row[1])
    return roi[:roi_height // 2, :]

def calculate_fps():
    global new_frame_time
    global prev_frame_time
    
    new_frame_time = time.time()

    if (new_frame_time-prev_frame_time) != 0:
        fps = 1 / (new_frame_time-prev_frame_time)
        prev_frame_time = new_frame_time
        return int(fps)

cv2.createTrackbar('Threshold Min', 'Frame', threshold_min, 255, update_threshold_min)
cv2.createTrackbar('Threshold Max', 'Frame', threshold_max, 255, update_threshold_max)
cv2.createTrackbar('Threshold h', 'Frame', threshold_hough, 255, update_threshold_hough)

def preprocess_image(img0: np.ndarray):
    """
    Preprocess image according to YOLOv7 input requirements.
    Takes image in np.array format, resizes it to specific size using letterbox resize, converts color space from BGR (default in OpenCV) to RGB and changes data layout from HWC to CHW.

    Parameters:
      img0 (np.ndarray): image for preprocessing
    Returns:
      img (np.ndarray): image after preprocessing
      img0 (np.ndarray): original image
    """
    # resize
    img = letterbox(img0, auto=False)[0]

    # Convert
    img = img.transpose(2, 0, 1)
    img = np.ascontiguousarray(img)
    return img, img0


def prepare_input_tensor(image: np.ndarray):
    """
    Converts preprocessed image to tensor format according to YOLOv7 input requirements.
    Takes image in np.array format with unit8 data in [0, 255] range and converts it to torch.Tensor object with float data in [0, 1] range

    Parameters:
      image (np.ndarray): image for conversion to tensor
    Returns:
      input_tensor (torch.Tensor): float tensor ready to use for YOLOv7 inference
    """
    input_tensor = image.astype(np.float32)  # uint8 to fp16/32
    input_tensor /= 255.0  # 0 - 255 to 0.0 - 1.0

    if input_tensor.ndim == 3:
        input_tensor = np.expand_dims(input_tensor, 0)
    return input_tensor


# label names for visualization
NAMES = ['teeth_good', 'no_teeth', 'teeth_bad']

# colors for visualization
COLORS = {'teeth_good': [255,255,0], 'no_teeth': [0,0,255], 'teeth_bad': [255,0,255]}

def detect(model: Model, image_path: Path, conf_thres: float = 0.7, iou_thres: float = 0.9, classes: List[int] = None, agnostic_nms: bool = False):
    """
    OpenVINO YOLOv7 model inference function. Reads image, preprocess it, runs model inference and postprocess results using NMS.
    Parameters:
        model (Model): OpenVINO compiled model.
        image_path (Path): input image path.
        conf_thres (float, *optional*, 0.25): minimal accpeted confidence for object filtering
        iou_thres (float, *optional*, 0.45): minimal overlap score for remloving objects duplicates in NMS
        classes (List[int], *optional*, None): labels for prediction filtering, if not provided all predicted labels will be used
        agnostic_nms (bool, *optiona*, False): apply class agnostinc NMS approach or not
    Returns:
       pred (List): list of detections with (n,6) shape, where n - number of detected boxes in format [x1, y1, x2, y2, score, label]
       orig_img (np.ndarray): image before preprocessing, can be used for results visualization
       inpjut_shape (Tuple[int]): shape of model input tensor, can be used for output rescaling
    """
    output_blob = model.output(0)
    img = image_path
    preprocessed_img, orig_img = preprocess_image(img)
    input_tensor = prepare_input_tensor(preprocessed_img)
    predictions = torch.from_numpy(model(input_tensor)[output_blob])
    pred = non_max_suppression(predictions, conf_thres, iou_thres, classes=classes, agnostic=agnostic_nms)
    return pred, orig_img, input_tensor.shape


def draw_boxes(predictions: np.ndarray, input_shape: Tuple[int], image: np.ndarray, names: List[str], colors: Dict[str, int]):
    """
    Utility function for drawing predicted bounding boxes on image
    Parameters:
        predictions (np.ndarray): list of detections with (n,6) shape, where n - number of detected boxes in format [x1, y1, x2, y2, score, label]
        image (np.ndarray): image for boxes visualization
        names (List[str]): list of names for each class in dataset
        colors (Dict[str, int]): mapping between class name and drawing color
    Returns:
        image (np.ndarray): box visualization result
    """
    if not len(predictions):
        return image, None
    # Rescale boxes from input size to original image size
    predictions[:, :4] = scale_coords(input_shape[2:], predictions[:, :4], image.shape).round()

    # Write results
    for *xyxy, conf, cls in reversed(predictions):
        label = f'{names[int(cls)]} {conf:.2f}'
        plot_one_box(xyxy, image, label=label, color=colors[names[int(cls)]], line_thickness=1)
    
    return image, predictions

core = Core()
# read converted model
model = core.read_model('model/tooth-detecter_int8.xml')
# load model on CPU device
compiled_model = core.compile_model(model, 'CPU')

frame_count = 0
while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break

    frame_count += 1
    # convert color from bgr to rgb
    frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    if frame_count % nr_of_skipping_frames == 0:
        boxes, image, input_shape = detect(compiled_model, frame_rgb)
        frame_rgb, predictions = draw_boxes(boxes[0], input_shape, image, NAMES, COLORS)        
        if predictions is not None:
            df = pd.DataFrame(predictions)

            if not df.empty:
                min_xmin_index = df[0].idxmin()
                row_with_least_xmin = df.loc[min_xmin_index]
                class_id_most_left = int(row_with_least_xmin[5])
            else:
                continue
            
            if class_id_most_left == 0 and not df.empty:
                roi = get_roi(row_with_least_xmin)
                lines, edge_frame = calculate_lines(roi)
                edge_frame = cv2.cvtColor(edge_frame, cv2.COLOR_GRAY2BGR)
                pt1, pt2 = get_leftmost_line(lines)
                if pt1 and pt2 is not None:
                    angle = calculate_angle_to_xaxis(pt1, pt2)
                    print('Angle', 90 - angle)

                draw_line(pt1, pt2, edge_frame)
                cv2.imshow('Tooth', edge_frame)

    cv2.imshow('Frame', frame_rgb)
    # calculate Frames
    new_frame_time = time.time()
    
    if (new_frame_time-prev_frame_time) != 0:
        fps = 1 / (new_frame_time-prev_frame_time)
        prev_frame_time = new_frame_time
        fps = int(fps)

    # print(fps)
                
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    

cap.release()
cv2.destroyAllWindows()

