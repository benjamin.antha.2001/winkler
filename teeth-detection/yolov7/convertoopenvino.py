from openvino.runtime import serialize
import openvino as ov
model = ov.convert_model('runs/train/yolov7-custom-Multiplelabels_V42/weights/best.onnx')
# serialize model for saving IR
ov.save_model(model, 'model/MultiLabel_V5/tooth-detecter.xml')