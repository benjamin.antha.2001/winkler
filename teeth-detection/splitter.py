import os
import random
import shutil

def split_data(image_dir, label_dir, train_dir, val_dir, test_dir, split_ratio=(0.7, 0.2, 0.1)):
    # Ensure the destination directories exist
    for directory in [train_dir, val_dir, test_dir]:
        if not os.path.exists(directory):
            os.makedirs(os.path.join(directory, "images"))
            os.makedirs(os.path.join(directory, "labels"))

    image_files = os.listdir(image_dir)
    random.shuffle(image_files)

    num_files = len(image_files)
    num_train = int(split_ratio[0] * num_files)
    num_val = int(split_ratio[1] * num_files)

    train_files = image_files[:num_train]
    val_files = image_files[num_train:num_train + num_val]
    test_files = image_files[num_train + num_val:]

    copy_files(train_files, image_dir, train_dir, "images")
    copy_files(val_files, image_dir, val_dir, "images")
    copy_files(test_files, image_dir, test_dir, "images")

    copy_label_files(train_files, label_dir, train_dir, "labels")
    copy_label_files(val_files, label_dir, val_dir, "labels")
    copy_label_files(test_files, label_dir, test_dir, "labels")

def copy_files(files, source_dir, dest_dir, subdir):
    for file in files:
        src = os.path.join(source_dir, file)
        dest = os.path.join(dest_dir, subdir, file)
        shutil.copy(src, dest)

def copy_label_files(image_files, label_dir, dest_dir, subdir):
    for image_file in image_files:
        label_file = os.path.splitext(image_file)[0] + ".txt"
        src = os.path.join(label_dir, label_file)
        dest = os.path.join(dest_dir, subdir, label_file)
        shutil.copy(src, dest)

# Example usage:
image_directory = "output_combined"    
label_directory = "annotations_v1"
train_directory = "train"
val_directory = "val"
test_directory = "test"

split_data(image_directory, label_directory, train_directory, val_directory, test_directory)
