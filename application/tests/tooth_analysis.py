import unittest
import cv2
import sys
import os
script_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(script_dir + "/../")
from modules.tooth_processing.line_analysis import LineAnalyser
from modules.tooth_processing.distance_analysis import DistanceAnalyser
from modules.tooth_processing.teeth_analyser import TeethAnalyser
import modules.constants as constants
import modules.detection_pipeline as detection_pipeline
import openvino.properties as props
from openvino.runtime import Core

model_path = '../model/MultiLabel_V3/tooth-detecter.xml'
core = Core()
model = core.read_model(model_path)
compiled_model = core.compile_model(model=model, device_name="AUTO", config={"PERFORMANCE_HINT": "LATENCY", props.inference_num_threads: "4"})
class TestToothAnalysis(unittest.TestCase):
    threshold_edge_min = 10
    threshold_edge_max = 40
    threshold_hough = 60
    ANGLE_SHOULD = 1.5605
    DISTANCE_SHOULD = 0.157
    TEETH_COUNT_SHOULD = 39
    
    img = cv2.imread(os.path.join(script_dir, 'images', 'output.png'))
    
    def test_angle(self):
        """Test calculating right angle"""
        height, width, _ = self.img.shape
        roi_tooth = self.img[0:height//2, 0:width]
        line_analyser = LineAnalyser(roi_tooth, self.threshold_edge_min, self.threshold_edge_max, self.threshold_hough)
        angle = line_analyser.calculate_angle_of_tooth()
        angle = round(angle, 4)
        self.assertEqual(angle, self.ANGLE_SHOULD)

    def test_distance(self):
        """Test calculating right distance"""
        height, width, _ = self.img.shape
        # Define the ROI coordinates for the bottom half
        roi_tooth = self.img[0:height//2, 0:width]
        roi_gullet = self.img[height//2:height, 0:width]
        distance_analyser = DistanceAnalyser(roi_gullet, roi_tooth, self.threshold_edge_min, self.threshold_edge_max, self.threshold_hough)
        result = distance_analyser.calculate_distance()
        distance = round(result, 4)
        self.assertEqual(distance, self.DISTANCE_SHOULD)
    
    def test_counting_teeth(self):
        """Test counting theeth of a blade"""
        teeth_analyser = TeethAnalyser(10, 20, 10, 20, 60)
        cap = cv2.VideoCapture('../tests/videos/video_testing_counting.mp4')
        frame_count = 0
        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                break

            frame_count += 1
            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            
            if frame_count % constants.SKIP_FRAMES == 0:
                boxes, image, input_shape = detection_pipeline.detect(compiled_model, frame_rgb, conf_thres=0.5)
                frame_rgb, predictions = detection_pipeline.draw_boxes(boxes[0], input_shape, image, constants.NAMES, constants.COLORS)
                if predictions is not None:
                    teeth_analyser.is_welding(predictions)
            
        cap.release()
        self.assertEqual(self.TEETH_COUNT_SHOULD, teeth_analyser.get_teeth_count())

    def test_detect_begin_of_blade(self):
        """Test if it detects the begin of an blade"""
        teeth_analyser = TeethAnalyser(10, 20, 10, 10, 60)
        cap = cv2.VideoCapture('../tests/videos/video_testing_begin.mp4')
        frame_count = 0
        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                break

            frame_count += 1
            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            
            if frame_count % constants.SKIP_FRAMES == 0:
                boxes, image, input_shape = detection_pipeline.detect(compiled_model, frame_rgb, conf_thres=0.5)
                frame_rgb, predictions = detection_pipeline.draw_boxes(boxes[0], input_shape, image, constants.NAMES, constants.COLORS)
                if predictions is not None:
                    teeth_analyser.is_welding(predictions)
            
        cap.release()
        self.assertTrue(teeth_analyser.get_begin_of_blade())

    def test_detect_end_of_blade(self):
        """Test if it detects the end of an blade"""
        teeth_analyser = TeethAnalyser(10, 20, 10, 20, 60)
        cap = cv2.VideoCapture('../tests/videos/video_testing_end.mp4')
        frame_count = 0
        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                break

            frame_count += 1
            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            
            if frame_count % constants.SKIP_FRAMES == 0:
                boxes, image, input_shape = detection_pipeline.detect(compiled_model, frame_rgb, conf_thres=0.5)
                frame_rgb, predictions = detection_pipeline.draw_boxes(boxes[0], input_shape, image, constants.NAMES, constants.COLORS)
                if predictions is not None:
                    teeth_analyser.is_end_of_blade(predictions, frame)
            
        cap.release()
        self.assertTrue(teeth_analyser.get_end())


if __name__ == "__main__":
    unittest.main(verbosity=2)