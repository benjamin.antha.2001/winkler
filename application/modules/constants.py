# Names for different detected objects
NAMES = ['teeth_good', 'no_teeth', 'teeth_bad', 'schweissen', 'elektrode_ein', 'elektrode_aus', 'greifer', 'schaft', 'spitz']

# Colors for each object name
COLORS = {
    'teeth_good': [0, 255, 0],      # Green
    'no_teeth': [240, 32, 160],     # Pink
    'teeth_bad': [0, 0, 255],       # Blue
    'schweissen': [255, 0, 255],    # Purple
    'elektrode_ein': [255, 255, 0], # Yellow
    'elektrode_aus': [0, 255, 255], # Cyan
    'greifer': [100, 100, 255],     # Light Blue
    'schaft': [100, 100, 100],      # Gray
    'spitz': [255, 255, 255]        # White
}

# Number of frames which are skipped
SKIP_FRAMES = 13