import cv2
import numpy as np
import math

HOUGH_MINIMAZER = 2

class LineAnalyser:
    points_of_line = None
    edge_frame = None
    _angle = None

    def __init__(self, roi, threshold_edge_min, threshold_edge_max, threshold_hough):
        self.roi = roi
        self.thresh_edge_min = threshold_edge_min
        self.thresh_edge_max = threshold_edge_max
        self.thresh_hough = threshold_hough

    def calculate_angle_of_tooth(self):
        angle = None
        points = self.detect_leftmost_line()
        # to do: check
        if points[0] is not None:
            angle = self.__calculate_angle_to_xaxis(points[0], points[1])

        return angle
    
    def detect_leftmost_line(self):
        lines = self.__calculate_lines()
        if lines is None:
            lines = self.__get_the_next_possible_line(self.thresh_hough - HOUGH_MINIMAZER, self.edge_frame, HOUGH_MINIMAZER)

        pt1, pt2 = self.__get_leftmost_line(lines)
        self.points_of_line = [pt1, pt2]
        return [pt1, pt2]
    
    def draw_angle(self, roi_detection):
        # to do: check 
        color = (0,0,255)
        if self.points_of_line[0] is not None:
            x1, y1 = self.points_of_line[0]
            x2, y2 = self.points_of_line[1]
            if 1 <= self._angle <= 5:
                color = (0,255,0)
            cv2.line(roi_detection, (int(x1), int(y1)), (int(x2), int(y2)) , color, 2, cv2.LINE_AA)
        return roi_detection
    
    def __calculate_lines(self):
        tooth_frame = cv2.cvtColor(self.roi, cv2.COLOR_BGR2GRAY)
        tooth_frame = cv2.GaussianBlur(tooth_frame, (9,9), 0)
        edge_frame = cv2.Canny(tooth_frame, self.thresh_edge_min, self.thresh_edge_max, None, 3)
        self.edge_frame = edge_frame
        return cv2.HoughLines(edge_frame, 1, 0.4 * np.pi / 180, self.thresh_hough ,min_theta = 2.5, max_theta=3.2)

    def __get_the_next_possible_line(self, threshold, edge_frame, hough_minimazer):
        lines = cv2.HoughLines(edge_frame, 1, 0.4 * np.pi / 180, threshold=threshold ,min_theta = 2.5, max_theta=3.2)
        if lines is None:
            if threshold > 0:
                return self.__get_the_next_possible_line(threshold - hough_minimazer, edge_frame, hough_minimazer)
            return None
        else:
            return lines
        
    def __get_leftmost_line(self, lines):
        leftmost_line = None
        leftmost_rho = float('inf')

        if lines is not None:
            for i in range(len(lines)):
                rho = lines[i][0][0]

                if rho < leftmost_rho:
                    leftmost_rho = rho
                    leftmost_line = lines[i]

        if leftmost_line is not None:
            rho = leftmost_line[0][0]
            theta = leftmost_line[0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0 = a * rho
            y0 = b * rho
            pt1 = ((x0 + 130*(-b)), (y0 + 130*(a)))
            pt2 = ((x0 - 130*(-b)), (y0 - 130*(a)))

            return (pt1, pt2)
        
        return (None, None)

    def __calculate_angle_to_xaxis(self, pt1, pt2):
        m = (pt2[1] - pt1[1]) / (pt2[0] - pt1[0])
        angle_in_rad = np.arctan(m)
        self._angle = 90 - abs(math.degrees(angle_in_rad))
        return self._angle
    
    def get_roi(self):
        return self.roi

    def set_roi(self, value):
        self.roi = value

    def get_edge_frame(self):
        return self.edge_frame
    
    def get_points_of_line(self):
        return self.get_points_of_line
