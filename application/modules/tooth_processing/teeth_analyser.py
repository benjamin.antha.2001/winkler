import pandas as pd
import modules.detection_pipeline as detection_pipeline
from .roi_helper import RoiHelper
from .line_analysis import LineAnalyser
from .distance_analysis import DistanceAnalyser
from datetime import datetime
import cv2
class TeethAnalyser:
    _teeth_counter = 0
    _is_welding = False
    _begin_of_blade = False
    _line_analyser = None
    _distance_analyser = None
    _roi_helper = None
    _is_end_of_blade = False
    _frame_spitz = None
    _current_frame = False
    _predictions_spitz = None
    _teeth_count = 0
    _edge_frame_tooth = None
    _edge_frame_gullet = None
    _result_data = []
    angle = None
    distance = None
    # for testing
    _end = False
    persistor = None
    save_results = False

    def __init__(self, tooth_thresh_min, tooth_thresh_max, gullet_thresh_min, gullet_thresh_max, threshold_hough):
        self.tooth_thresh_edge_min = tooth_thresh_min
        self.tooth_thresh_edge_max = tooth_thresh_max
        self.gullet_thresh_edge_min = gullet_thresh_min
        self.gullet_thresh_edge_max = gullet_thresh_max
        self.threshold_hough = threshold_hough

    def is_welding(self, predictions):
        df = pd.DataFrame(predictions)
        if not df[df[5] ==  3].empty and not df[df[5] ==  5].empty:
            if not self._is_welding:
                self._teeth_counter += 1

            self._is_welding = True
        else:
            self._is_welding = False

        self.is_begin_of_blade(predictions)

        return self._is_welding
    
    def is_begin_of_blade(self, predictions):
        df = pd.DataFrame(predictions)

        if not df[df[5] == 7].empty:
            if not self._begin_of_blade:
                print('...... counter reset ......', self._teeth_counter)
                self._teeth_count = self._teeth_counter
                self._teeth_counter = 0
            self._begin_of_blade = True
        else:
            self._begin_of_blade = False
    
    def is_end_of_blade(self, predictions, frame):
        df = pd.DataFrame(predictions)
        df = df[(df[5] != 7) & (df[5] != 5)]
        # exclude half detections
        df = df[((df[2] - df[0]) >= 80)]
        # df = df[df[4] >= 0.7]

        # check if is not welding
        if not df[df[5] ==  3].empty or not df[df[5] ==  5].empty:
            return self._is_end_of_blade

        # check for no tooth
        if not df[df[5] ==  1].empty:
            for row in df[df[5] ==  1].itertuples(index=False, name='Pandas'):
                if row[0] > 960:
                    return self._is_end_of_blade
                
        if not df[df[5] == 8].empty:
            self._current_frame = True
            self._frame_spitz = frame
            self._predictions_spitz = df
        else:
            if self._current_frame:
                self._is_end_of_blade = True
                self._end = True
                print('......end of Blade ......')
            else:
                self._is_end_of_blade = False

            self._current_frame = False
        
        return self._is_end_of_blade
        
    def handle_end_of_blade(self):
        df = self._predictions_spitz[(self._predictions_spitz[5] == 0) | (self._predictions_spitz[5] == 1) | (self._predictions_spitz[5] == 2)]
        sorted_df = df.sort_values(by=0)
        calculations = []

        for row in sorted_df.itertuples(index=False, name='Pandas'):
            self._roi_helper = RoiHelper(row, self._frame_spitz)
            if (row[5] == 0):
                results = self.__tooth_analysis()
                calculations.append(results)
            elif (row[5] == 1):
                calculations.append(['n', 'n'])
            elif (row[5] == 2):
                calculations.append(['b', 'b'])

        self.__handle_last_results(calculations)
        if self.save_results:
            self.__persist_blade_results()
        
    def tooth_calculations(self, predictions, frame):
        df = pd.DataFrame(predictions)
        most_left_detection = detection_pipeline.get_most_left_detection(df)
        class_id_most_left = int(most_left_detection[5])

        if class_id_most_left == 0 and self._teeth_counter != 2:
            self._roi_helper = RoiHelper(most_left_detection, frame)
            
            angle, distance = self.__tooth_analysis()
            self.__add_results_to_array(angle, distance)

            return self.__draw_results()
        
        return None
    
    def __tooth_analysis(self):
        # roi's
        roi_tooth = self._roi_helper.get_upper_half_roi()
        roi_gullet = self._roi_helper.get_bottom_half_roi()

        self._line_analyser = LineAnalyser(roi_tooth, self.tooth_thresh_edge_min, self.tooth_thresh_edge_max, self.threshold_hough)
       
        # angle detection
        self.angle = round(self._line_analyser.calculate_angle_of_tooth(), 4)
        self._edge_frame_tooth = self._line_analyser.get_edge_frame()
        # to do: check
        if self.angle:
            print('angle:', self.angle)
            
        # distance detection
        self._distance_analyser = DistanceAnalyser(roi_gullet, roi_tooth, self.gullet_thresh_edge_min, self.gullet_thresh_edge_max, self.threshold_hough)
        if self._line_analyser.get_points_of_line():
            self.distance = self._distance_analyser.calculate_distance(self._line_analyser.get_points_of_line(), self._line_analyser.get_edge_frame())
            self.distance = round(self.distance, 4)
            print('distance:', self.distance)
        else:
            self.distance = self._distance_analyser.calculate_distance()
            self.distance = round(self.distance, 4)
            print('distance:', self.distance)
        
        self._edge_frame_gullet = self._distance_analyser.line_analyser.get_edge_frame()

        return [self.angle, self.distance]
    
    def __add_results_to_array(self, angle, distance):
        current_datetime = datetime.now()
        current_date = current_datetime.strftime('%Y-%m-%d')
        current_time = current_datetime.strftime('%H:%M:%S.%f')[:-3]
        if angle is None:
            angle = 'x'
        if distance is None:
            distance = 'x'

        row = [current_date, current_time, self._teeth_counter - 2, angle, distance]
        self._result_data.append(row)

    def __persist_blade_results(self):
        typ = 'x'
        self.persistor.read_tips_per_types()
        type_of_blade = [row for row in self.persistor.tips_per_type if row[2] == str(self._teeth_counter)]
        
        if type_of_blade != []:
            self._result_data = [sublist + [type_of_blade[0][0]] for sublist in self._result_data]
            self._result_data = [sublist + [type_of_blade[0][1]] for sublist in self._result_data]
        else:
            self._result_data = [sublist + [typ] for sublist in self._result_data]
            self._result_data = [sublist + [typ] for sublist in self._result_data]
        
        headers = ['date', 'time', 'nr', 'angle', 'overhang','sku','type']
        self._result_data.insert(0, headers)
        self.persistor.write_data(self._result_data)
        # reset data
        self._result_data = []
        
    def __handle_last_results(self, calculations):
        print(calculations)
        current_datetime = datetime.now()
        current_date = current_datetime.strftime('%Y-%m-%d')
        current_time = current_datetime.strftime('%H:%M:%S.%f')[:-3]
        # iterate over the last two teeths
        i = 1
        for angle, distance in calculations[-2:]:
            row = [current_date, current_time, self._teeth_counter -  i, angle, distance]
            self._result_data.append(row)
            i -= i

    def __draw_results(self):
        roi_detection = self._roi_helper.get_roi()
        roi_detection = self._line_analyser.draw_angle(roi_detection)
        roi_detection = self._distance_analyser.drawing_distance(roi_detection)
        # cv2.imwrite('uberstand.png', roi_detection)
        return roi_detection
    
    def set_tresholds(self, tooth_thresh_min, tooth_thresh_max, gullet_thresh_min, gullet_thresh_max, threshold_hough):
        self.tooth_thresh_edge_min = tooth_thresh_min
        self.tooth_thresh_edge_max = tooth_thresh_max
        self.gullet_thresh_edge_min = gullet_thresh_min
        self.gullet_thresh_edge_max = gullet_thresh_max
        self.threshold_hough = threshold_hough

    def get_edges(self):
        return self._edge_frame_tooth, self._edge_frame_gullet
    
    def get_begin_of_blade(self):
        return self._begin_of_blade
    
    def get_teeth_count(self):
        return self._teeth_count
    
    def get_end(self):
        return self._end