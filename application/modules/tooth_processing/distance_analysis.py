import numpy as np
from .line_analysis import LineAnalyser
import cv2

class DistanceAnalyser:
    PIXEL_IN_MM = 0.01083
    _middle_x_of_gullet = 0
    _lowest_point = [0,0]
    _highest_point = [0,0]
    _middle_point_of_tooth = []
    _distance = None

    def __init__(self, roi_gullet, roi_tooth, threshold_edge_min, threshold_edge_max, threshold_hough):
        self.roi_gullet = roi_gullet
        self.roi_tooth = roi_tooth
        self.thresh_edge_min = threshold_edge_min
        self.thresh_edge_max = threshold_edge_max
        self.thresh_hough = threshold_hough
        self.line_analyser = LineAnalyser(roi_tooth, threshold_edge_min, threshold_edge_max, threshold_hough)

    def calculate_distance(self, points_of_line_tooth = None, edges_tooth = None):
        # tooth point
        if points_of_line_tooth or edges_tooth is None:
            points_of_line_tooth = self.line_analyser.detect_leftmost_line()

        points_on_edges = self.__get_edge_points_near_line(points_of_line_tooth, self.line_analyser.get_edge_frame())
        if points_on_edges == []:
            return self._distance
        
        lowest_point = self.__get_lowest_point(points_on_edges)
        highest_point = self.__get_highest_point(points_on_edges)
        middle_x_of_tooth = (lowest_point[0] + highest_point[0]) / 2
        midlle_y_of_tooth = (lowest_point[1] + highest_point[1]) / 2
        
        # Gullet point
        self.line_analyser.set_roi(self.roi_gullet)
        points_of_line_gullet = self.line_analyser.detect_leftmost_line()
        points_on_edges_gullet = self.__get_edge_points_near_line(points_of_line_gullet, self.line_analyser.get_edge_frame())    
        if points_on_edges_gullet == []:
            return self._distance

        # calculate distance
        middle_x_of_gullet = self.__average_x(points_on_edges_gullet)
        distance_in_pixel = middle_x_of_gullet - middle_x_of_tooth

        self._middle_point_of_tooth = [int(middle_x_of_tooth),int(midlle_y_of_tooth)]
        self._distance = self.__convert_pixel_to_mm(distance_in_pixel)

        return self._distance
    
    def drawing_distance(self, roi_detection):
        color = (0,0,255)
        if 0.15 <= self._distance <= 0.35:
            color = (0,255,0)
            
        cv2.line(roi_detection, (self._middle_x_of_gullet, self._lowest_point[1]), (self._middle_x_of_gullet, 400), (0,0,0), 1, cv2.LINE_AA)
        cv2.line(roi_detection, (self._middle_point_of_tooth[0], self._lowest_point[1]), (self._middle_point_of_tooth[0], 400), (0,0,0), 1, cv2.LINE_AA)
        cv2.line(roi_detection, (self._middle_point_of_tooth[0], 160), (self._middle_x_of_gullet, 160), color, 2, cv2.LINE_AA)

        # cv2.circle(roi_detection, self._middle_point_of_tooth, radius=4, color=(0, 0, 255), thickness=-1)
        return roi_detection
    
    def __get_edge_points_near_line(self, points_of_line, edges):
        pt1, pt2 = points_of_line
        points = []
        if pt1 is not None:
            A = np.array(pt1)
            v = np.array([pt2[0] - pt1[0], pt2[1] - pt1[1]])
            v_lenth = np.sqrt(v[0]**2 + v[1]**2)
            for y, x in zip(*np.nonzero(edges)):
                P = np.array([x,y])
                AP = P - A
                d = abs(np.cross(AP, v) / v_lenth)
                if d < 1:
                    points.append([x,y])
            
        return points

    def __get_lowest_point(self, points):
        # to do: check
        if points:
            self._lowest_point = max(points, key=lambda point: point[1]) 
        return self._lowest_point
    
    def __get_highest_point(self, points):
        # to do: check
        highest_point = []
        if points:
            highest_point = min(points, key=lambda point: point[1]) 
        return highest_point

    def __convert_pixel_to_mm(self, pixel):
        return pixel * self.PIXEL_IN_MM

    def __average_x(self, points):
        if not points:
            return 0
        points_array = np.array(points)
        self._middle_x_of_gullet = int(round(np.mean(points_array[:, 0])))
        return self._middle_x_of_gullet

