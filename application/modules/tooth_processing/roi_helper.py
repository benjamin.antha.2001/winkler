class RoiHelper:
    def __init__(self, row, frame):
        self.row = row
        self.frame = frame

    def get_roi(self):
        return self.frame[int(self.row[1]):int(self.row[3]), int(self.row[0]): int(self.row[2])]

    def get_bottom_half_roi(self):
        roi_height = int(self.row[3]) - int(self.row[1])
        bottom_half_roi = self.frame[int(self.row[1]) + roi_height // 2:int(self.row[3]), int(self.row[0]):int(self.row[2])]
        return bottom_half_roi

    def get_upper_half_roi(self):
        roi = self.frame[int(self.row[1]):int(self.row[3]), int(self.row[0]): int(self.row[2])]
        roi_height = int(self.row[3]) - int(self.row[1])
        return roi[:roi_height // 2, :]