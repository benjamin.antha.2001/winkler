from PySide6.QtWidgets import QWidget, QLabel, QVBoxLayout, QSlider, QHBoxLayout, QCheckBox, QPushButton, QFileDialog
from PySide6.QtCore import Qt, Slot
from PySide6.QtGui import QPixmap, QImage
from ..persistor.persistor import CsvPersistor

LABEL_NAMES = ['Min', 'Max']

class ThresholdWindow(QWidget):
    def __init__(self, thread, is_threshold_window_open):
        super().__init__()
        self.title = 'Settings'
        self.th = thread
        self.is_threshold_window_open = is_threshold_window_open
        self.persistor = CsvPersistor()
        self.input_values = self.persistor.read_data()
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setMinimumWidth(500)

        layout = QVBoxLayout()

        # Create Tooth section
        tooth_title = QLabel("Tooth")
        tooth_title.setAlignment(Qt.AlignCenter)
        layout.addWidget(tooth_title)

        for name in LABEL_NAMES:
            self.add_slider_with_label(layout, name, 'tooth')

        # Add tooth edge frame
        self.label_tooth = QLabel(self)
        self.label_tooth.setScaledContents(True)
        self.label_tooth.setFixedSize(150, 150)
        self.th.edge_tooth_pixmap.connect(self.setEdgeTooth)
        layout.addWidget(self.label_tooth)

        # Create Gullet section
        gullet_title = QLabel("Gullet")
        gullet_title.setAlignment(Qt.AlignCenter)
        layout.addWidget(gullet_title)

        for name in LABEL_NAMES:
            self.add_slider_with_label(layout, name, 'gullet')

        # Add Gullet edge frame
        self.label_gullet = QLabel(self)
        self.label_gullet.setScaledContents(True)
        self.label_gullet.setFixedSize(150, 150)
        self.label_gullet.setFocus()
        self.th.edge_gullet_pixmap.connect(self.setEdgeGullet)
        layout.addWidget(self.label_gullet)

        # Add checkbox and buttons for saving results
        self.checkbox_layout = QHBoxLayout()
        self.file_checkbox = QCheckBox("Save Results")
        self.file_checkbox.setChecked(eval(self.input_values['saveResults']))
        self.file_checkbox.stateChanged.connect(self.toggle_file_button)

        self.file_button = QPushButton("Choose Path")
        self.file_button.setVisible(eval(self.input_values['saveResults']))
        self.file_button.clicked.connect(self.open_file_dialog)

        self.checkbox_layout.addWidget(self.file_checkbox)
        self.checkbox_layout.addWidget(self.file_button)
        layout.addLayout(self.checkbox_layout)

        self.label_path = QLabel(self.input_values['selected_path'])
        self.label_path.setAlignment(Qt.AlignRight)
        self.label_path.setVisible(eval(self.input_values['saveResults']))
        layout.addWidget(self.label_path)

        self.load_tips_button = QPushButton("Load Tips CSV")
        self.load_tips_button.setVisible(eval(self.input_values['saveResults']))
        self.load_tips_button.clicked.connect(self.open_csv_dialog)

        layout.addWidget(self.load_tips_button)
        self.label_path_tips = QLabel(self.input_values['selected_path_tip'])
        self.label_path_tips.setAlignment(Qt.AlignRight)
        self.label_path_tips.setVisible(eval(self.input_values['saveResults']))
        layout.addWidget(self.label_path_tips)
        

        self.setLayout(layout)

    def add_slider_with_label(self, layout, name, kind):
        slider_layout = QHBoxLayout()

        slider = QSlider(Qt.Orientation.Horizontal)
        slider.setRange(0, 255)
        slider.setSingleStep(1)
    
        if kind == 'tooth':
            if name == LABEL_NAMES[0]:
                slider.setValue(int(self.input_values["min_thresh_tooth"]))
            elif name == LABEL_NAMES[1]:
                slider.setValue(int(self.input_values['max_thresh_tooth']))

            label = QLabel(f"{name}: {slider.value()}")
            label.setAlignment(Qt.AlignCenter)
            slider.valueChanged.connect(lambda value: self.setToothThresholdValues(value, name, label))

        elif kind == 'gullet':
            if name == LABEL_NAMES[0]:
                slider.setValue(int(self.input_values['min_thresh_gullet']))
            elif name == LABEL_NAMES[1]:
                slider.setValue(int(self.input_values['max_thresh_gullet']))

            label = QLabel(f"{name}: {slider.value()}")
            label.setAlignment(Qt.AlignCenter)
            slider.valueChanged.connect(lambda value: self.setGulletThresholdValues(value, name, label))
    
        slider_layout.addWidget(label)
        slider_layout.addWidget(slider)

        layout.addLayout(slider_layout)
    
    @Slot(QImage)
    def setEdgeTooth(self, image):
        self.label_tooth.setPixmap(QPixmap.fromImage(image))
    
    @Slot(QImage)
    def setEdgeGullet(self, image):
        self.label_gullet.setPixmap(QPixmap.fromImage(image))

    @Slot()
    def setToothThresholdValues(self, value, name, label):
        if name == LABEL_NAMES[0]:
                self.th.setToothThreshMin(value)
                self.input_values['min_thresh_tooth'] = value
        elif name == LABEL_NAMES[1]:
                self.th.setToothThreshMax(value)
                self.input_values['max_thresh_tooth'] = value

        label.setText(f"{name}: {value}")

    @Slot()
    def setGulletThresholdValues(self, value, name, label):
        if name == LABEL_NAMES[0]:
                self.th.setGulletThreshMin(value)
                self.input_values['min_thresh_gullet'] = value
        elif name == LABEL_NAMES[1]:
                self.th.setGulletThreshMax(value)
                self.input_values['max_thresh_gullet'] = value

        label.setText(f"{name}: {value}")

    @Slot()
    def setHoughValue(self, value):
         self.th.setThreshHough(int(value))
         self.input_values['hough_thresh'] = int(value)

    @Slot()
    def toggle_file_button(self, state):
        self.file_button.setVisible(state)
        self.label_path.setVisible(state)
        self.label_path_tips.setVisible(state)
        self.load_tips_button.setVisible(state)
        self.input_values['saveResults'] = state == 2
        self.th.save_results = state == 2


    @Slot()
    def open_file_dialog(self):
        folderpath = QFileDialog.getExistingDirectory(self, 'Select Folder')
        if folderpath:
            self.th.saving_result_path = folderpath
            self.input_values['selected_path'] = folderpath
            self.label_path.setText(folderpath)
    
    @Slot()
    def open_csv_dialog(self):
        options = QFileDialog.Options()
        file_path, _ = QFileDialog.getOpenFileName(self, "Select Path", "", "CSV Files (*.csv)", options=options)
        if file_path:
            self.th.filename_tips_per_type = file_path
            self.input_values['selected_path_tip'] = file_path
            self.label_path_tips.setText(file_path)

    def closeEvent(self, event):
        self.is_threshold_window_open = False
        self.persistor.update_input_values(self.input_values)
        self.th.setEmitEdges(False)
        event.accept()
