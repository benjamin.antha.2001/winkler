from PySide6.QtWidgets import QWidget, QLabel, QGridLayout, QMainWindow
from PySide6.QtCore import Qt, Slot
from PySide6.QtGui import QImage, QPixmap, QAction, QFont
from .threshold_window import ThresholdWindow
from .name_change_window import NameChangeWindow
from ..worker_teeth import Thread
from ..persistor.persistor import CsvPersistor

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.persistor = CsvPersistor()
        self.input_values = self.persistor.read_data()
        self.is_setup_window_open = False 
        self.is_name_change_window_open = False
        self.updated = False
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.input_values['name'])
        self.setMinimumSize(640, 480)
        
        # Central widget and layout
        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)
        layout = QGridLayout(central_widget)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        # Video label
        self.label_video = QLabel(self)
        self.label_video.setScaledContents(True)
        layout.addWidget(self.label_video, 0, 0)
        
        # ROI label (absolute positioning)
        self.label_roi = QLabel(self)
        self.label_roi.setScaledContents(True)
        self.label_roi.setFixedSize(150, 150)
        self.label_roi.setStyleSheet("background: transparent; border: 2px solid black;")

        self.label_angle = QLabel("Winkel: ",self)
        self.label_angle.setFixedSize(240, 50)
        self.label_angle.setScaledContents(True)
        self.label_angle.setFont(QFont('Arial', 22)) 
        self.label_angle.setStyleSheet("background: gray; border: 2px solid black;")

        self.label_distance = QLabel("Distanz: ",self)
        self.label_distance.setFixedSize(240, 50)
        self.label_distance.setScaledContents(True)
        self.label_distance.setFont(QFont('Arial', 22)) 
        self.label_distance.setStyleSheet("background: gray; border: 2px solid black;")

        # menu buttons
        btn_change_thresh = QAction("Change Setup", self)
        btn_change_thresh.triggered.connect(self.openThresholdWindow)

        btn_show_results =  QAction("Change Name", self)
        btn_show_results.triggered.connect(self.openChangeNameWindow)

        # menu bar
        menu = self.menuBar()
        settings_menu = menu.addMenu("Winkler")
        settings_menu.addAction(btn_change_thresh)
        settings_menu.addAction(btn_show_results)

        # Start the thread
        self.th = Thread(self)
        # connection to worker thread
        self.th.video_pixmap.connect(self.setVideoImage)
        self.th.roi_pixmap.connect(self.setRoiImage)
        self.th.angle.connect(self.setAngle)
        self.th.distance.connect(self.setDistance)

        self.th.save_results = bool(self.input_values['saveResults'])
        self.th.saving_result_path = self.input_values['selected_path']
        self.th.filename_tips_per_type = self.input_values['selected_path_tip']
        self.th.start()
        self.showMaximized()

    @Slot()
    def openThresholdWindow(self):
        if not self.is_setup_window_open:
            self.th.setEmitEdges(True)
            self.threshold_window = ThresholdWindow(self.th, self.is_setup_window_open)
            self.threshold_window.show()
        else:
            self.th.setEmitEdges(False)
            self.threshold_window.close()
            self.threshold_window = None
    
    @Slot()
    def openChangeNameWindow(self):
        if not self.is_name_change_window_open:
            self.change_name_window = NameChangeWindow(self, self.is_name_change_window_open)
            self.change_name_window.show()
        else:
            self.change_name_window.close()
            self.change_name_window = None

    def resizeEvent(self, event):
        super().resizeEvent(event)
        self.updateOverlayPosition()

    def updateOverlayPosition(self):
        x = self.width() - self.label_roi.width()
        y = 0
        self.label_roi.move(x, y)

    def updateOverlayPositionAngleText(self):
        x = self.width() - self.label_angle.width()
        y = self.label_roi.height()
        self.label_angle.move(x, y)
        self.updated = True

    def updateOverlayPositionDistanceText(self):
        x = self.width() - self.label_distance.width()
        y = self.label_roi.height() + self.label_angle.height()
        self.label_distance.move(x, y)
        self.updated = True

    @Slot(QImage)
    def setVideoImage(self, image):
        image = image.scaled(self.size(), Qt.KeepAspectRatio)
        self.label_video.setPixmap(QPixmap.fromImage(image))

    @Slot(QImage)
    def setRoiImage(self, roi):
        roi = roi.scaled(400, 400, Qt.KeepAspectRatio)
        self.label_roi.setPixmap(QPixmap.fromImage(roi))
        self.label_roi.setFixedSize(roi.size())
        self.updateOverlayPosition()
    
    @Slot(float)
    def setAngle(self, angle):
        self.updateOverlayPositionAngleText()
        if 1 <= angle <= 5:
            self.label_angle.setStyleSheet("background: green; border: 2px solid black;")
        else:
            self.label_angle.setStyleSheet("background: red; border: 2px solid black;")

        self.label_angle.setText(f"Winkel: {angle}")
    
    @Slot(float)
    def setDistance(self, distance):
        self.updateOverlayPositionDistanceText()
        if 0.15 <= distance <= 0.35:
            self.label_distance.setStyleSheet("background: green; border: 2px solid black;")
        else:
            self.label_distance.setStyleSheet("background: red; border: 2px solid black;")

        self.label_distance.setText(f"Distanz: {distance}")


