from PySide6.QtWidgets import QWidget, QLabel, QVBoxLayout, QHBoxLayout, QPushButton, QLineEdit
from PySide6.QtCore import Slot
from ..persistor.persistor import CsvPersistor

class NameChangeWindow(QWidget):
    def __init__(self, parent, is_name_change_window_open):
        super().__init__()
        self.title = 'Change Name'
        self.parent_window = parent
        self.is_name_change_window_open = is_name_change_window_open
        self.persistor = CsvPersistor()
        self.input_values = self.persistor.read_data()
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setMinimumWidth(500)

        layout = QVBoxLayout()

        label_name = QLabel("Enter a new Name")
        self.input_name = QLineEdit()
        self.input_name.setText(self.input_values['name'])
        self.input_name.textChanged.connect(self.name_changed)

        layout.addWidget(label_name)
        layout.addWidget(self.input_name)

        self.ok_button = QPushButton("OK")
        self.cancel_button = QPushButton("Cancel")

        self.ok_button.clicked.connect(self.on_ok_clicked)
        self.cancel_button.clicked.connect(self.on_cancel_clicked)

        button_layout = QHBoxLayout()
        button_layout.addWidget(self.ok_button)
        button_layout.addWidget(self.cancel_button)

        layout.addLayout(button_layout)
        self.setLayout(layout)

    @Slot(str)
    def name_changed(self, value):
        self.input_name.setText(value)
        self.input_values['name'] = value
    
    @Slot()
    def on_ok_clicked(self):
        self.persistor.update_input_values(self.input_values)
        self.is_name_change_window_open = False
        self.parent_window.setWindowTitle(self.input_values['name'])
        self.close()

    @Slot()
    def on_cancel_clicked(self):
        self.is_name_change_window_open = False
        self.close()

    def closeEvent(self, event):
        self.is_name_change_window_open = False
        event.accept() # let the window close
    