import cv2
import openvino.properties as props
from openvino.runtime import Core
from PySide6.QtCore import QThread, Qt, Signal
from PySide6.QtGui import QImage
import modules.detection_pipeline as detection_pipeline
import time
from .tooth_processing.teeth_analyser import TeethAnalyser
import numpy as np
import modules.constants as constants
from .persistor.persistor import CsvPersistor

model_path = 'model/MultiLabel_V4/tooth-detecter_int8.xml'

class Thread(QThread):
    video_pixmap = Signal(QImage)
    roi_pixmap = Signal(QImage)
    edge_tooth_pixmap = Signal(QImage)
    edge_gullet_pixmap = Signal(QImage)
    angle = Signal(float)
    distance = Signal(float)
    roi_img = np.zeros((1000, 500, 3), dtype = np.uint8)
    prev_frame_time = 0
    new_frame_time = 0
    hough_minimazer = 2
    core = Core()
    frame_count = 0
    model = core.read_model(model_path)
    compiled_model = core.compile_model(model=model, device_name="AUTO", config={"PERFORMANCE_HINT": "LATENCY", props.inference_num_threads: "4"})
    tooth_thresh_min = 10
    tooth_thresh_max = 20
    gullet_thresh_min = 10
    gullet_thresh_max = 20
    thresh_hough = 60
    emit_edges = False
    skipping_frames = constants.SKIP_FRAMES
    saving_result_path = 'results'
    save_results = False
    filename_tips_per_type = None
    
    def run(self): 
        self.isRunning=True
        cap = cv2.VideoCapture('videos/video_05.mp4')
        self.teeth_analyser = TeethAnalyser(self.tooth_thresh_min, self.tooth_thresh_max, self.gullet_thresh_min, self.gullet_thresh_max, self.thresh_hough)
        self.persistor = CsvPersistor()
        fps = cap.get(cv2.CAP_PROP_FPS)
        delay = int(1000 / fps)

        while self.isRunning:
            self.teeth_analyser.set_tresholds(self.tooth_thresh_min, self.tooth_thresh_max, self.gullet_thresh_min, self.gullet_thresh_max, self.thresh_hough)
            self.persistor.filename_tips_per_type = self.filename_tips_per_type
            self.persistor.change_path(self.saving_result_path)
            self.teeth_analyser.persistor = self.persistor
            self.teeth_analyser.save_results = self.save_results

            ret, frame = cap.read()
            if not ret:
                break

            self.frame_count += 1
            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            
            # skip frames to boost performance
            if self.frame_count % constants.SKIP_FRAMES == 0:
                self.analyse_frame(frame, frame_rgb)
            
            self.calculate_fps(frame_rgb)
            self.emit_results(frame_rgb)
            QThread.msleep(delay)

        cap.release()

    def analyse_frame(self, frame, frame_rgb):
        boxes, image, input_shape = detection_pipeline.detect(self.compiled_model, frame_rgb, conf_thres=0.5)
        frame_rgb, predictions = detection_pipeline.draw_boxes(boxes[0], input_shape, image, constants.NAMES, constants.COLORS)
        if predictions is not None:
            is_end_of_blade = self.teeth_analyser.is_end_of_blade(predictions, frame)
            is_welding = self.teeth_analyser.is_welding(predictions)

            if is_welding:
                self.roi_img = self.teeth_analyser.tooth_calculations(predictions, frame)
                if self.roi_img is not None:
                    self.roi_img = cv2.cvtColor(self.roi_img, cv2.COLOR_BGR2RGB)

            elif is_end_of_blade:
                self.teeth_analyser.handle_end_of_blade()

    def emit_results(self, frame_rgb):
        frame_rgb = self.convert_to_qt_format(frame_rgb)
        self.video_pixmap.emit(frame_rgb)

        if isinstance(self.roi_img, np.ndarray):
            self.roi_img = self.convert_to_qt_format(self.roi_img)
            self.roi_pixmap.emit(self.roi_img)  
            self.angle.emit(self.teeth_analyser.angle)
            self.distance.emit(self.teeth_analyser.distance)          
        
        if self.emit_edges:
            tooth_edge, gullet_edge = self.teeth_analyser.get_edges()
            if tooth_edge is not None and gullet_edge is not None:
                tooth_edge = cv2.cvtColor(tooth_edge, cv2.COLOR_GRAY2RGB)
                tooth_edge = self.convert_to_qt_format(tooth_edge)

                gullet_edge = cv2.cvtColor(gullet_edge, cv2.COLOR_GRAY2RGB)
                gullet_edge = self.convert_to_qt_format(gullet_edge)

                self.edge_tooth_pixmap.emit(tooth_edge)
                self.edge_gullet_pixmap.emit(gullet_edge)


    def stop(self):
        self.isRunning=False
        self.quit()
        self.terminate()

    def convert_to_qt_format(self, frame):
        h, w, ch = frame.shape
        bytes_per_line = ch * w
        convert_to_qtformat = QImage(frame.data, w, h, bytes_per_line, QImage.Format_RGB888)
        return convert_to_qtformat.scaled(640, 480, Qt.KeepAspectRatio)
    
    def calculate_fps(self, frame):
        self.new_frame_time = time.time()
        if (self.new_frame_time - self.prev_frame_time) != 0:
            fps = 1 / (self.new_frame_time-self.prev_frame_time)
            self.prev_frame_time = self.new_frame_time
            # print(int(fps))
            self.draw_fps(fps, frame)
    
    def draw_fps(self, fps, frame):
        font = cv2.FONT_HERSHEY_SIMPLEX
        org = (25,100)
        font_scale = 1
        color = (0,255,0)
        thickness = 2
        fps_formatted = "%.2f" % fps
        text = "FPS: " + fps_formatted
        cv2.putText(frame, text, org, font, font_scale, color, thickness, cv2.LINE_AA)

    def setToothThreshMin(self, value):
        self.tooth_thresh_min = value
    
    def setToothThreshMax(self, value):
        self.tooth_thresh_max = value

    def setGulletThreshMin(self, value):
        self.gullet_thresh_min = value
    
    def setGulletThreshMax(self, value):
        self.gullet_thresh_max = value
    
    def setEmitEdges(self, value):
        self.emit_edges = value
    
    def setThreshHough(self, value):
        self.thresh_hough = value
    