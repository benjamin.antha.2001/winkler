import csv
import os
from datetime import datetime

class CsvPersistor:
    tips_per_type = []
    def __init__(self):
        self.path = 'results'
        self.filename_results = self.path + '/' + datetime.now().strftime('%Y-%m-%d-%H-%M-%S') + '.csv'
        self.filename_inputs = 'modules/gui/inputs.csv'
        self.filename_tips_per_type = 'modules/gui/tips_per_type.csv'
        self.header_written = False
        self._inputs_values = []
        self.read_tips_per_types()
    
    def change_path(self, path):
        if path != self.path:
            self.filename_results = path + '/' + datetime.now().strftime('%Y-%m-%d-%H-%M-%S') + '.csv'
            self.path = path

    def write_data(self, data):
        if os.path.exists(self.filename_results):
            self.header_written = True

        with open(self.filename_results, mode='a', newline='') as file:
            writer = csv.writer(file)
            
            if not self.header_written:
                writer.writerow(data[0])  
                self.header_written = True
            writer.writerows(data[1:])
    
    def read_data(self):
        with open(self.filename_inputs, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                self._inputs_values.append(row)

            return self._inputs_values[0]
        
    def read_tips_per_types(self):
        with open(self.filename_tips_per_type, encoding='utf-8-sig', mode='r') as file:
            csv_reader = csv.reader(file, delimiter=';')
            self.tips_per_type = [row for row in csv_reader]

        
    def update_input_values(self, input_values):
        with open(self.filename_inputs, mode='w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(input_values.keys())
            writer.writerow(input_values.values())

    