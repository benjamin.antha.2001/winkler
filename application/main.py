from PySide6.QtWidgets import QApplication
from modules.gui.main_window import MainWindow
import sys

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    app.aboutToQuit.connect(ex.th.stop) #stop qthread when closing window
    sys.exit(app.exec())