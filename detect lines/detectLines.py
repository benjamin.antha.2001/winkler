import sys
import math
import cv2 as cv
import numpy as np

def sharpen_image(image):
    kernel = np.array([[-1, -1, -1],
                       [-1, 9, -1],
                       [-1, -1, -1]])
    return cv.filter2D(image, -1, kernel)


def main(argv):
    default_file = 'output.png'
    filename = argv[0] if len(argv) > 0 else default_file
    # Loads an image
    src = cv.imread(cv.samples.findFile(filename), cv.IMREAD_GRAYSCALE)
    src = cv.GaussianBlur(src, (5,5), 0) 

    # Check if image is loaded fine
    if src is None:
        print("Error opening image!")
        print("Usage: hough_lines.py [image_name -- default " + default_file + "] \n")
        return -1
    
    
    dst = cv.Canny(src, 30, 60, None, 3)
    
    # Copy edges to the images that will display the results in BGR
    cdst = cv.cvtColor(dst, cv.COLOR_GRAY2BGR)
    cdstP = np.copy(cdst)
    
    lines = cv.HoughLines(dst, 1, np.pi / 180, 30)
    
    if lines is not None:
        for i in range(0, len(lines)):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0 = a * rho
            y0 = b * rho
            pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
            pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
            cv.line(cdst, pt1, pt2, (0,0,255), 1, cv.LINE_AA)
    
    
    linesP = cv.HoughLinesP(dst, 1, np.pi / 180, 30, None, 60, 10)
    
    
    if linesP is not None:
        for i in range(0, len(linesP)):
            l = linesP[i][0]
            p1 = [l[0], l[1]]
            p2 = [l[2], l[3]]
            m = (p2[1] - p1[1]) / (p2[0] - p1[0])
            angle_in_rad = np.arctan(m)
            angle_in_deg = math.degrees(angle_in_rad)
            angle_to_vertical_line = 90 - angle_in_deg
            print("p1", p1)
            print("p2", p2)
            print("m", m)
            print("angle",abs(angle_to_vertical_line))
            cv.line(cdstP, (l[0], l[1]), (l[2], l[3]), (255,0,0), 1, cv.LINE_AA) 

    
    cv.imshow("Source", src)
    cv.imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst)
    cv.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)
    
    cv.waitKey()
    return 0
 
if __name__ == "__main__":
    main(sys.argv[1:])